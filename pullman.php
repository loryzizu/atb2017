<?php
require_once(  __DIR__.'/vendor/autoload.php');
$userID = isset($_POST["userID"]) ? intval($_POST["userID"]) : null;
$accessToken = isset($_POST["accessToken"]) ? $_POST["accessToken"] : null;
$hiddenPlace = isset($_POST["hiddenPlace"]) && trim($_POST["hiddenPlace"])!='' ? trim($_POST["hiddenPlace"]) : null;

$fb = new Facebook\Facebook([
    'app_id' => '1544040812312893',
    'app_secret' => '859f7a5c1dff17759caf4655bba2817b',
    'default_graph_version' => 'v2.2',
]);

try {
    // Returns a `Facebook\FacebookResponse` object
    $response = $fb->get('/me?fields=id,name,email', $accessToken);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}
$user = $response->getGraphUser();
$email= $user['email'];
$name= $user['name'];
if($userID!=null && $hiddenPlace !=null && $email != null && $name!=null ) {
    $con = mysqli_connect("localhost", "d6j5c7e1", "f7u7l9t5", "atb");
    $sql = "SELECT userId FROM navetta WHERE userId=?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param('i', $userID);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_array();
    if ($row != null) {
        $sql = "UPDATE navetta SET  email = ?, place = ? , name = ? WHERE userId =?;";
        $stmt = $con->prepare($sql);
        $stmt->bind_param('sssi', $email, $hiddenPlace,$name, $userID);
    } else {
        $sql = "INSERT INTO navetta (userId, email, place, name) VALUES (?, ?,?,?);";
        $stmt = $con->prepare($sql);
        $stmt->bind_param('isss', $userID, $email, $hiddenPlace, $name);
    }

    $result = $stmt->execute();
    echo $stmt->error;

    if ($result) {
        $con->commit();
    }

    echo $result;
}


