create database atb;

CREATE TABLE atb.navetta
			(
			userId INT,
			email VARCHAR(255),
			place VARCHAR(255)
			);
ALTER TABLE atb.navetta MODIFY userId LONG;